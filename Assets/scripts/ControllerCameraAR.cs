﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerCameraAR : MonoBehaviour {

    public GameObject StartScreen;
    public GameObject startAnimatation;
    public Animator animator;
    public Transform objectoanimator;

	// Use this for initialization
	void Start () {
        animator = objectoanimator.GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void closeArAnimation(){
        startAnimatation.SetActive(false);
        StartScreen.SetActive(true);
    }

    public void playAnimation(){
        animator.Play("Run");
    }
}
