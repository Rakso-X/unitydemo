﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartController : MonoBehaviour {

	public GameObject btnStart;
    public GameObject panelDialogInstrucciones;
    public GameObject panelStartScreen;
    public GameObject cameraAr;


    void Start() {
        //Ocultando el Panel de Instucciones
        panelDialogInstrucciones.SetActive(false);
        panelStartScreen.SetActive(true);
        cameraAr.SetActive(false);
        Screen.orientation = ScreenOrientation.Portrait;
    }
	
	// Update is called once per frame
	void Update ()
    {
        Screen.orientation = ScreenOrientation.Portrait;

    }

    public void pressBtnInstrucciones(){
        panelDialogInstrucciones.SetActive(true);
    }

    public void closeDialogOverlay(){
        panelDialogInstrucciones.SetActive(false);
    }

    public void startARAnimatione(){
        cameraAr.SetActive(true);
        panelStartScreen.SetActive(false);
    }

    public void closeARAnimation(){
        cameraAr.SetActive(false);
        panelStartScreen.SetActive(false);
    }

    public void changeScene(){
        SceneManager.LoadScene("GameScene");
    }

}
